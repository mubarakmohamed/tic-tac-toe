var boxes = document.querySelectorAll("td");
var boxtext = document.querySelectorAll("td span");
var player = document.querySelector("#player");
var playerTurnDisplay = document.querySelector("#playerTurnDisplay");
var resetButton = document.querySelector("#resetButton");
var isXTurn = true;
var boxesLeftToClick = 9;
var gameEnd = false;

function init() {
  addClickListeners(boxes);
}

function addClickListeners(arr) {
    for (var i = 0; i < arr.length; i++) {
      arr[i].addEventListener("click", function() {
        if (isEmpty(this) && gameEnd === false) {
          addObjectToBox(this);
          isXTurn = !(isXTurn);
          boxesLeftToClick--;
          displayTurn();
          checkWinner(arr);
        }
      });
      resetButton.addEventListener("click", function() {
        reset();
      })
    }
  }
  // la boîte est-elle vide [true] ou contient-elle un X ou un O [false]
function isEmpty(box) {
    if (box.textContent !== "X" && box.textContent !== "O")
      return true;
    else
      return false;
  }
  
  // ajoute l'objet "X" au tableau

  function addCross(box) {
    box.classList.add("cross");
    box.textContent = "X";
  }
  
  // ajoute l'objet "O" au tableau
function addCircle(box) {
    box.classList.add("circle");
    box.textContent = "O";
  }

  //affiche à qui le tour est donné aux joueurs

function displayTurn() {
    if (boxesLeftToClick > 0) {
      player.textContent = (isXTurn) ? "X" : "O";
      playerTurnDisplay.classList.toggle("xtour");
      playerTurnDisplay.classList.toggle("ytour");
      }
        else {
      playerTurnDisplay.textContent = "Nulle ET Marci";
      playerTurnDisplay.classList.toggle(playerTurnDisplay.classList);
      playerTurnDisplay.classList.add("gameEnd");
    }
  }
  //si quelqu'un gagne, supprime la couleur de fond et affiche le texte de celui qui a gagné
  
function setWinnerDisplay(box, orientation) {
  playerTurnDisplay.classList.remove("xtour");
  playerTurnDisplay.classList.remove("ytour");
    playerTurnDisplay.classList.add("gameEnd");
  playerTurnDisplay.textContent = box.textContent + " gagne " + orientation;
    gameEnd = true;
  }
  
  //ajoute la "ligne" à travers les cases gagnantes
function crossOut(box1, box2, box3) {
    box1.classList.add("crossOut");
    box2.classList.add("crossOut");
    box3.classList.add("crossOut");
  }
  //toutes les choses qui doivent arriver quand un joueur gagne se produiront si vous appelez cette fonction
function win(box1, box2, box3, orientation) {
    setWinnerDisplay(box1, orientation);
    crossOut(box1, box2, box3);
  }
  //retourne vrai si les cases sont toutes des X ou des O si elles sont vides ou pas identiques
function isTheSame(box1, box2, box3) {
  if (box1.textContent === box2.textContent && box1.textContent === box3.textContent && isEmpty(box1) === false) 
  return true;
  else
  return false;
}
function reset() {
    for (var i = 0; i < boxes.length; i++) {
      boxes[i].textContent = "";
      isXTurn = true;
      boxes[i].classList.remove("crossOut");
      boxes[i].classList.remove("gameEnd");
    }
    gameEnd = false;
  if (isXTurn) playerTurnDisplay.classList.add("xtour");
    boxesLeftToClick = 9;
  }
//vérifie un tableau de 9 éléments pour voir si leur textContent est le même
function checkWinner(arr) {
  if (isTheSame(arr[0], arr[1], arr[2]))
    win(arr[0], arr[1], arr[2], "horizontalement");
  else if (isTheSame(arr[3], arr[4], arr[5]))
    win(arr[3], arr[4], arr[5], "horizontalement");
  else if (isTheSame(arr[6], arr[7], arr[8]))
    win(arr[6], arr[7], arr[8], "horizontalement");
  else if (isTheSame(arr[0], arr[3], arr[6]))
    win(arr[0], arr[3], arr[6], "verticalement");
  else if (isTheSame(arr[1], arr[4], arr[7]))
    win(arr[1], arr[4], arr[7], "verticalement");
  else if (isTheSame(arr[2], arr[5], arr[8]))
    win(arr[2], arr[5], arr[8], "verticalement");
  else if (isTheSame(arr[0], arr[4], arr[8]))
    win(arr[0], arr[4], arr[8], "en diagonale");
  else if (isTheSame(arr[2], arr[4], arr[6]))
    win(arr[2], arr[4], arr[6], "en diagonale");
}
function addObjectToBox(box) {
  if (isXTurn)
    addCross(box);
  else
    addCircle(box);
}
init();
